import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hybrid_mobile_application_task_3/Models/computer.dart';
import 'package:hybrid_mobile_application_task_3/Repositories/StoreRepositoryImplm.dart';
import 'package:hybrid_mobile_application_task_3/bloc/store_bloc.dart';
import 'package:hybrid_mobile_application_task_3/bloc/store_event.dart';
import 'package:hybrid_mobile_application_task_3/bloc/store_state.dart';
import 'package:hybrid_mobile_application_task_3/create_entry_page.dart';
import 'package:hybrid_mobile_application_task_3/entry_page.dart';
import 'package:hybrid_mobile_application_task_3/update_entry_page.dart';

class MyItemsPage extends StatefulWidget {
  @override
  _MyItemsPageState createState() => _MyItemsPageState();
}

class _MyItemsPageState extends State<MyItemsPage> {
    final StoreBloc _storeBloc =
      StoreBloc(storeRepository: StoreRepositoryImplementation());

  @override
  void initState() {
    _storeBloc.dispatch(StreamStore());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
      return StreamBuilder(
      initialData: _storeBloc.initialState,
      stream: _storeBloc.state,
      builder: (context, AsyncSnapshot<StoreState> snapshot) {
        return Scaffold(
          appBar: AppBar(
            title: Text("My Listings"),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => CreateEntry()));
            },
            child: Icon(Icons.add),
            backgroundColor: Colors.red,
          ),
          body: ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
            shrinkWrap: true,
            padding: const EdgeInsets.all(20.0),
            itemCount: _storeBloc.currentState.storeComputers.length,
            itemBuilder: (context, index) {
              List<Computer> computers = _storeBloc.currentState.storeComputers;
              String title = computers[index].name;
              String price = computers[index].price.toString() + "€";
              String imageUrl = computers[index].imageUrl;
              // print(_storeBloc.currentState.storeComputers.length);
              // Computer computer = Computer( "oof","Dull xps 15", 500, "Nice computer, great shape.", "https://sm.pcmag.com/t/pcmag_ap/review/d/dell-xps-1/dell-xps-15-7590-oled_f7n7.1200.jpg");
              // _storeBloc.dispatch(AddItemToStore(computer));
              //print('id:' + computers[index].id);
              return ListTile(
                title: Text(title),
                subtitle: Text(price),
                leading: CircleAvatar(
                  radius: 30,
                  backgroundImage: NetworkImage(imageUrl),
                  backgroundColor: Colors.transparent,
                ),
                trailing: IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () {
                    _showDialog(computers[index]);
                  },
                ),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => UpdateEntryPage(computer:  computers[index],)));
                },
              );
            },
          ),
        );
      },
    );
  }

   void _showDialog(Computer computer) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Do you want to delete this item from your listings?"),
          content:Text("This item will be deleted from the listings permanently"),
          actions: <Widget>[
            FlatButton(
              child: Text("Delete"),
              onPressed: () {
                _storeBloc.dispatch(DeleteItemFromStore(computer));
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}