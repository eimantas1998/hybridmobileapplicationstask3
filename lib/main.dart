import 'package:flutter/material.dart';
import 'package:hybrid_mobile_application_task_3/myItems.dart';
import 'package:hybrid_mobile_application_task_3/storePage.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int _selectedPage = 0;
  final _pageOptions = [
    StorePage(),
    MyItemsPage()
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: Scaffold(
        body: _pageOptions[_selectedPage],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedPage,
          onTap: (int index) {
            setState(() {
              _selectedPage = index;
            });
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.computer),
              title: Text('Store'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('My Items'),
            )
          ],
        ),
      ),
    );
  }
}
