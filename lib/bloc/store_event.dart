import 'package:hybrid_mobile_application_task_3/Models/computer.dart';

abstract class StoreEvent {}

class AddItemToStore extends StoreEvent {
  final Computer computer;

  AddItemToStore(this.computer);
}

class DeleteItemFromStore extends StoreEvent {
  final Computer computer;

  DeleteItemFromStore(this.computer);
}

class UpdateStoreItem extends StoreEvent {
  final Computer computer;

  UpdateStoreItem(this.computer);
}

class StreamStore extends StoreEvent {}

class SyncStore extends StoreEvent {
  final List<Computer> storeComputers;

  SyncStore(this.storeComputers);
}




