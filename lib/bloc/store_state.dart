import 'package:hybrid_mobile_application_task_3/Models/computer.dart';
import 'package:meta/meta.dart';

class StoreState {
  final bool isLoading;
  final String error;
  final List<Computer> storeComputers;

  const StoreState({
    @required this.isLoading,
    @required this.error,
    @required this.storeComputers,
  });

  factory StoreState.initial(){
    return StoreState(
      isLoading: false,
      error: '',
      storeComputers: []
    );
  }

  StoreState copyWith({
    bool isLoading,
    String error,
    List<Computer> storeComputers
  }) {
    return StoreState(
      isLoading: isLoading ?? this.isLoading,
      error: error ?? this.error,
      storeComputers: storeComputers ?? this.storeComputers
    );
  }
}

