import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:hybrid_mobile_application_task_3/Repositories/StoreRepository.dart';
import './bloc.dart';

class StoreBloc extends Bloc<StoreEvent, StoreState> {
  final StoreRepository _storeRepository;
  StreamSubscription _storeSubscription;

  StoreBloc({@required StoreRepository storeRepository})
      : assert(storeRepository != null),
        _storeRepository = storeRepository;
        

  @override
  void dispose(){
    super.dispose();
    _storeSubscription?.cancel();
  }

  @override
  StoreState get initialState => StoreState.initial();

  @override
  Stream<StoreState> mapEventToState(StoreEvent event) async* {

    if(event is AddItemToStore){
      _storeRepository.addNewStoreEntry(event.computer);
    }
    else if(event is DeleteItemFromStore){
      _storeRepository.deleteStoreEntry(event.computer);
    }
    else if(event is UpdateStoreItem){
     _storeRepository.updateStoreEntry(event.computer); 
    }
    else if(event is StreamStore){
      try{
        yield currentState.copyWith(error: '', isLoading: true);
        //_storeSubscription.cancel();
        _storeSubscription = _storeRepository
          .streamEntries().listen(
            (computers) {
              dispatch(SyncStore(computers));
              print(computers.length);
            },
          );
        yield currentState.copyWith(error: '', isLoading: false);
      } catch (e) {
        print("StoreBloc - StreamStore - $e");
        yield currentState.copyWith(error: '$e');
        yield currentState.copyWith(error: '', isLoading: false);
      }
      
    }
    else if(event is SyncStore){
      print(event.storeComputers);
      yield currentState.copyWith(storeComputers: event.storeComputers);
    }
  }
}
