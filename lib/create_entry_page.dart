import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hybrid_mobile_application_task_3/Models/computer.dart';
import 'package:hybrid_mobile_application_task_3/Repositories/StoreRepositoryImplm.dart';
import 'package:hybrid_mobile_application_task_3/bloc/store_bloc.dart';
import 'package:hybrid_mobile_application_task_3/bloc/store_event.dart';

class CreateEntry extends StatefulWidget {
  @override
  _CreateEntryState createState() => _CreateEntryState();
}

class _CreateEntryState extends State<CreateEntry> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    final StoreBloc _storeBloc =
      StoreBloc(storeRepository: StoreRepositoryImplementation());
      
  String name;
  String price;
  String desciption;
  String imageUrl;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
            title: Text("Create a listing"),
          ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _submitForm();
        },
        label: Text('Add to listings'),
        backgroundColor: Colors.red,
      ),
      body: SafeArea(
        top: false,
        bottom: false,
        child: Form(
          key: _formKey,
          autovalidate: true,
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            children: <Widget>[
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.computer),
                  hintText: 'Enter computers model',
                  labelText: 'Model'
                ),
                validator: (val) => val.isEmpty ? "Model is required" : null,
                onSaved: (value) => name = value,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.monetization_on),
                  hintText: 'Enter price',
                  labelText: 'Price'
                ),
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                validator: (val) => isValidPrice(val) ? null : "Price is required",
                onSaved: (value) => price = value,
                inputFormatters: [
                  WhitelistingTextInputFormatter(RegExp(r'\d+\.?\d*')),
                ],
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.image),
                  hintText: 'Enter image url',
                  labelText: 'Image Url'
                ),
                validator: (val) => val.isEmpty ? "Url cant be empty" : null,
                onSaved: (value) => imageUrl = value,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.description),
                  hintText: 'Enter description, like specs and number to call',
                  labelText: 'Description'
                ),
                validator: (val) => val.isEmpty ? "Description is required" : null,
                onSaved: (value) => desciption = value,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitForm(){
    final FormState form = _formKey.currentState;
    form.save();
    if(form.validate()){
       num realPrice = double.parse(price);
       Computer computer = Computer('', name, realPrice, desciption, imageUrl);
      _storeBloc.dispatch(AddItemToStore(computer));
      _showAddedDialog();
    } else {
      _showErrorDialog();
    }
  }

   void _showAddedDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Success"),
          content:Text("The listing has been added sucesfully"),
          actions: <Widget>[
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
   }

   void _showErrorDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Error"),
          content:Text("Theres something missing check and try again"),
          actions: <Widget>[
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
   }

  //  bool isValidPrice(String input) {
  //   var urlPattern = r"(https?|http)://([-A-Z0-9.]+)(/[-A-Z0-9+&@#/%=~_|!:,.;]*)?(\?[A-Z0-9+&@#/%=~_|!:‌​,.;]*)?";
  //   RegExp regex = RegExp(urlPattern);
  //   return regex.hasMatch(input);
  // }

  bool isValidPrice(String input) {
    final RegExp regex = RegExp(r'\d+\.?\d*');
    return regex.hasMatch(input);
  }
}