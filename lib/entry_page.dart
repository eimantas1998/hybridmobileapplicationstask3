import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hybrid_mobile_application_task_3/Models/computer.dart';

class EntryPage extends StatefulWidget {
  final Computer computer;
  EntryPage(this.computer);
  @override
  _EntryPageState createState() => _EntryPageState();
}

String value = "";

class _EntryPageState extends State<EntryPage> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.computer.name),
          backgroundColor: Colors.red,
        ),
        body: CustomScrollView(
            slivers: <Widget>[
               SliverToBoxAdapter(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Center(
                          child: Image.network(
                            widget.computer.imageUrl,
                            height: 300,
                            width: 300,
                            fit:BoxFit.fill
                            ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SliverToBoxAdapter(
                child: SizedBox(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Center(
                      child: Text("Description",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                    ),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: SizedBox(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Center(
                      child: Text(widget.computer.description,
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontSize: 17
                          ),),
                    ),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  alignment: Alignment.centerRight,
                  child: SizedBox(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text(widget.computer.price.toString() + '€',
                            style: TextStyle(
                                fontSize: 20, 
                                fontWeight: FontWeight.bold,
                                color: Colors.green)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
  }
}