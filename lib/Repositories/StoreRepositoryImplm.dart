import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hybrid_mobile_application_task_3/Models/computer.dart';
import 'package:hybrid_mobile_application_task_3/Models/computerEntity.dart';
import 'package:hybrid_mobile_application_task_3/Repositories/StoreRepository.dart';

//https://medium.com/flutter-community/firestore-todos-with-flutter-bloc-7b2d5fadcc80

class StoreRepositoryImplementation implements StoreRepository {
  final storeCollection = Firestore.instance.collection('Store');

  @override
  Future<void> addNewStoreEntry(Computer computer) {
    return storeCollection.add(computer.toEntity().toDocument());
  }

  @override
  Future<void> deleteStoreEntry(Computer computer) async {
    return storeCollection.document(computer.id).delete();
  }

  @override
  Stream<List<Computer>> streamEntries() {
    return storeCollection.snapshots().map((snapshot) {
      return snapshot.documents
          .map((doc) => Computer.fromEntity(ComputerEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Future<void> updateStoreEntry(Computer update) {
    return storeCollection
        .document(update.id)
        .updateData(update.toEntity().toDocument());
  }}