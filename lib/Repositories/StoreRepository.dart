import 'package:hybrid_mobile_application_task_3/Models/computer.dart';

// https://medium.com/flutter-community/firestore-todos-with-flutter-bloc-7b2d5fadcc80

abstract class StoreRepository {
  Future<void> addNewStoreEntry(Computer computer);

  Future<void> deleteStoreEntry(Computer computer);

  Future<void> updateStoreEntry(Computer computer);

  Stream<List<Computer>> streamEntries();
}