import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class ComputerEntity extends Equatable {
  final String id;
  final String name;
  final num price;
  final String description;
  final String imageUrl;

  ComputerEntity(this.id, this.name, this.description, this.price, this.imageUrl);

  Map<String, Object> toJson(){
    return {
      'id': id,
      'name': name,
      'description': description,
      'price': price,
      'imageUrl': imageUrl,
    };
  }  

  @override
  String toString() {
    return 'ComputerEntity { id: $id, name: $name, price: $price, description: $description,  imageUrl: $imageUrl}';
  }

  static ComputerEntity fromJson(Map<String, Object> json) {
    return ComputerEntity(
      json['id'] as String,
      json['name'] as String,
      json['description'] as String,
      json['price'] as num,
      json['imageUrl'] as String,
    );
  }

  static ComputerEntity fromSnapshot(DocumentSnapshot snap) {
    return ComputerEntity(
      snap.documentID,
      snap.data['name'],
      snap.data['description'],
      snap.data['price'],
      snap.data['imageUrl']
    );
  }

  Map<String, Object> toDocument() {
    return {
      'name': name,
      'description': description,
      'price': price,
      'imageUrl': imageUrl
    };
  }
}