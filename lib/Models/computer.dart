import 'package:hybrid_mobile_application_task_3/Models/computerEntity.dart';

class Computer {
  final String id;
  final String name;
  final num price;
  final String description;
  final String imageUrl;

  Computer(
    this.id,
    this.name,
    this.price,
    this.description,
    this.imageUrl,
  );

  factory Computer.empty() {
    return Computer( '', '',  0, '', '');
  }

  Computer copyWith({
    String id,
    String name,
    num price,
    String description,
    String imageUrl,
  }) {
    return Computer(
       id ?? this.id,
       name ?? this.name,
       price ?? this.price,
       description ?? this.description,
       imageUrl ?? this.imageUrl);
  }
 
   @override
   int get hashCode => 
    id.hashCode ^ name.hashCode ^ price.hashCode ^ description.hashCode ^ imageUrl.hashCode;

   @override
   bool operator ==(Object other) =>
      identical(this, other) || 
      other is Computer &&
            runtimeType == other.runtimeType &&
            id == other.id &&
            name == other.name &&
            price == other.price &&
            description == other.description &&
            imageUrl == other.imageUrl;

  @override
  String toString() {
    return 'Computer {id: $id, name: $name, price: $price, descryption: $description, imageUrl: $imageUrl}';
  }

  ComputerEntity toEntity() {
    return ComputerEntity(id, name, description, price, imageUrl);
  }

  static Computer fromEntity(ComputerEntity entity){
    return Computer(
      entity.id,
      entity.name,
      entity.price,
      entity.description,
      entity.imageUrl
    );
  }
}
