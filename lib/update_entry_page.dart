import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hybrid_mobile_application_task_3/Models/computer.dart';
import 'package:hybrid_mobile_application_task_3/Repositories/StoreRepositoryImplm.dart';
import 'package:hybrid_mobile_application_task_3/bloc/store_bloc.dart';
import 'package:hybrid_mobile_application_task_3/bloc/store_event.dart';

class UpdateEntryPage extends StatefulWidget {
  @override
  _UpdateEntryPageState createState() => _UpdateEntryPageState();
  final Computer computer;

  UpdateEntryPage({@required this.computer});
}

class _UpdateEntryPageState extends State<UpdateEntryPage> {

  final StoreBloc _storeBloc =
      StoreBloc(storeRepository: StoreRepositoryImplementation());
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String name;
  String price;
  String desciption;
  String imageUrl;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
            title: Text("Update ${widget.computer.name}"),
          ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          _submitForm();
        },
        label: Text('Update Listing'),
        backgroundColor: Colors.red,
      ),
      body: SafeArea(
        top: false,
        bottom: false,
        child: Form(
          key: _formKey,
          autovalidate: true,
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            children: <Widget>[
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.computer),
                  hintText: 'Enter computers model',
                  labelText: 'Model'
                ),
                initialValue: widget.computer.name,
                validator: (val) => val.isEmpty ? "Model is required" : null,
                onSaved: (value) => name = value,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.monetization_on),
                  hintText: 'Enter price',
                  labelText: 'Price'
                ),
                initialValue: widget.computer.price.toString(),
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                validator: (val) => isValidPrice(val) ? null : "Price is required",
                onSaved: (value) => price = value,
                inputFormatters: [
                  WhitelistingTextInputFormatter(RegExp(r'\d+\.?\d*')),
                ],
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.image),
                  hintText: 'Enter image url',
                  labelText: 'Image Url'
                ),
                initialValue: widget.computer.imageUrl,
                validator: (val) => val.isEmpty ? "Url cant be empty" : null,
                onSaved: (value) => imageUrl = value,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.description),
                  hintText: 'Enter description, like specs and number to call',
                  labelText: 'Description'
                ),
                initialValue: widget.computer.description,
                validator: (val) => val.isEmpty ? "Description is required" : null,
                onSaved: (value) => desciption = value,
              ),
            ],
          ),
        ),
      ),
    );
  }

   void _submitForm(){
    final FormState form = _formKey.currentState;
    form.save();
    if(form.validate()){
       num realPrice = double.parse(price);
       Computer computer = Computer(widget.computer.id, name, realPrice, desciption, imageUrl);
      _storeBloc.dispatch(UpdateStoreItem(computer));
      _showAddedDialog();
    } else {
      _showErrorDialog();
    }
  }

   void _showAddedDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Success"),
          content:Text("The listing has been updated!"),
          actions: <Widget>[
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
   }

   void _showErrorDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Error"),
          content:Text("Theres something missing check and try again"),
          actions: <Widget>[
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
   }

    bool isValidPrice(String input) {
    final RegExp regex = RegExp(r'\d+\.?\d*');
    return regex.hasMatch(input);
  }
}